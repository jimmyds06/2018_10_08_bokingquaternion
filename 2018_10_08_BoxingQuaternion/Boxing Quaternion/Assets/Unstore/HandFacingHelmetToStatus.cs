﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HandFacingHelmetToStatus : MonoBehaviour {
    
    public Transform m_hand;
    public Vector3 m_directionForward;
    public Vector3 m_directionUpward;
    public Vector3 m_euler;
    public float m_verticalAngle;
    public float m_horizontalAngle;

    public HandState m_state = HandState.Unknow;
    public enum HandState { Unknow, Resting, Protecting, Hitting}

    public float m_protectingToleerance = 0.8f;
    public float m_hittingTolerance = 0.45f;


    public Text m_debugState;



    void Update () {

        m_directionForward = GetLocalDirectionForward(m_hand);
        m_directionUpward = GetLocalDirectionUp(m_hand);

        m_euler = m_hand.localRotation.eulerAngles;

        if (m_directionForward.y > m_protectingToleerance)
        {
            m_state = HandState.Protecting;
        }
        else {

            if (m_directionUpward.y > -m_hittingTolerance && m_directionUpward.y < m_hittingTolerance)
            {
                m_state = HandState.Hitting;
            }
            else {
                m_state = HandState.Resting;

            }

        }

        m_debugState.text = m_state.ToString();



        //OLD TRY
        Vector3 horizontal = GetLocalDirectionUp(m_hand);
        horizontal.z = 0;
        Vector3 vertical= GetLocalDirectionUp(m_hand);
        vertical.x = 0;
        m_horizontalAngle = Vector3.SignedAngle(Vector3.up, horizontal, Vector3.back);
        m_verticalAngle = Vector3.SignedAngle(Vector3.up, vertical, Vector3.right);


        




    }

    private Vector3 GetLocalDirectionUp(Transform hand)
    {
        return hand.parent.InverseTransformDirection(hand.up);
    }
    private Vector3 GetLocalDirectionForward(Transform hand)
    {
        return hand.parent.InverseTransformDirection(hand.forward);
    }
}
